var express = require('express');
var router = express.Router();

const { request, gql } = require('graphql-request');

// Định nghĩa endpoint GraphQL của Hasura
const endpoint = 'http://localhost:8080/v1/graphql'; // Thay đổi URL tương ứng với Hasura của bạn

// Định nghĩa truy vấn GraphQL để kiểm tra kết nối
const queryGetContacts = gql`
query {
  contact {
    id
    name
    age
  }
}
`;
// Định nghĩa truy vấn GraphQL cho việc thêm contact
const mutationAddContact = gql`
  mutation ($name: String!, $age: String!) {
    insert_contact(objects: { name: $name, age: $age }) {
      affected_rows
    }
  }
`;


// Middleware để kiểm tra kết nối GraphQL
router.get('', async function(req, res, next) {
  try {
    // Thực hiện truy vấn đơn giản đến GraphQL để kiểm tra kết nối
    const data = await request(endpoint, queryGetContacts);

    // Kiểm tra kết quả trả về có chứa dữ liệu không
    if (data) {
      // Nếu kết nối thành công, in ra console "success"
      console.log('success');
      res.render('index',{ title: 'Hêllo world'});
    } else {
      // Nếu kết nối không thành công, gửi lại phản hồi "failure"
      res.send('failure');
    }
  } catch (error) {
    // Xử lý lỗi nếu có
    console.error('Error checking GraphQL connection:', error);
    res.send('failure');
  }
});



// Gửi truy vấn GraphQL đến Hasura và lấy danh sách contact
const getContacts = async () => {
  try {
    const data = await request(endpoint, queryGetContacts);
    return data.contact;
  } catch (error) {
    console.error('Error fetching contacts:', error);
    throw error;
  }
};

// Thêm contact mới vào cơ sở dữ liệu
const addContact = async (name, age) => {
  try {
    const variables = { name, age };
    const data = await request(endpoint, mutationAddContact, variables);
    return data.insert_contact.affected_rows > 0;
  } catch (error) {
    console.error('Error adding contact:', error);
    throw error;
  }
};

/* GET thêm page. */
router.get('/them', async function(req, res, next) {
  res.render('them', { title: 'them' });
});
/* POST thêm page. */
router.post('/them', async function(req, res, next) {
  var name = req.body.name; 
  var age = req.body.age; 

  try {
    if (age !== null && age !== undefined) {
      age = age.toString(); // Chỉ chuyển đổi thành chuỗi nếu age không null hoặc undefined
    } else {
      throw new Error('Invalid age value');
    }

    // Thêm contact mới vào cơ sở dữ liệu
    const success = await addContact(name, age);
    if (success) {
      res.redirect('/xem');
    } else {
      throw new Error('Failed to add contact');
    }
  } catch (error) {
    console.error('Error adding contact:', error);
    res.send('Error adding contact');
  }
});



/* GET xem page. */
router.get('/xem', async function(req, res, next) {
  try {
    // Gửi truy vấn GraphQL để lấy danh sách contact từ Hasura
    const contacts = await getContacts();
    res.render('xem', { title: 'xem', data: contacts });
  } catch (error) {
    console.error('Error fetching data:', error);
    res.send('Error fetching data');
  }
});


module.exports = router;
